#include "dbusproxyobject.h"
#include <private/qmetaobjectbuilder_p.h>
#include <QtDBus>

DbusProxyObject::DbusProxyObject(const QString &interface, QObject *parent) :
    QObject(parent)
{
    m_proxyName = "DBusObjectProxy";

    m_remoteApp = new QDBusInterface( interface, "/", "org.freedesktop.DBus" );

    QMetaObjectBuilder builder;
    builder.setSuperClass(&QObject::staticMetaObject);
    builder.setClassName(m_proxyName);  //Should be unique to the interfcace i guess
    builder.setFlags(QMetaObjectBuilder::DynamicMetaObject);


    QDBusInterface introspector( "org.freedesktop.DBus", "/", "org.freedesktop.DBus.Introspectable" );
    QDBusReply<QString>  xml = introspector.call("Introspect");
    // Process XML to make the following happen for each introspected method/property/signal, code to do this in qdbusviewer
    qDebug() << "Yay XML!" << xml;

    QMetaMethodBuilder firstmethod = builder.addMethod("NameHasOwner(QString)", "bool");
    firstmethod.setAccess(QMetaMethod::Public);
    firstmethod.setParameterNames( QList<QByteArray>() << "argument1");

    QMetaMethodBuilder secondmethod = builder.addMethod("ListNames()", "QStringList");
    secondmethod.setAccess(QMetaMethod::Public);
    ///

    m_myMeta = builder.toMetaObject();

}

DbusProxyObject::~DbusProxyObject()
{
    delete m_myMeta;
}

void DbusProxyObject::classBegin()
{

}

void DbusProxyObject::componentComplete()
{

}

/** fake meta object glue **/
const QMetaObject *DbusProxyObject::metaObject() const
{
    return m_myMeta;
}

int DbusProxyObject::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;

    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2) // Number of methods ( get from QMetaObject
        {
            if ( _id == 0 ) // should come from metaobject method name to index
            {
                qDebug() << Q_FUNC_INFO << "Dbus method";
                 QVariantList args;
                 // _a[0] is return value
                 // _a[1:] are args
                 args << QVariant(QMetaType::QString, _a[1]); // Need to get the meta type from MetaMethod, We can ONLY handle QVariants here, and really only QVariants that go to and from JSON
                 QDBusMessage reply = m_remoteApp->callWithArgumentList(QDBus::BlockWithGui, "NameHasOwner",args); // method name from index, args from signature to QVariants
                 // in QML we should realy put this to a signal and then invoke a callback when finished for methods
                 if (reply.type() == QDBusMessage::ErrorMessage) {
                     qDebug() << Q_FUNC_INFO << reply.errorMessage();
                     *reinterpret_cast<bool*>(_a[0]) = 0; // this should really throw an exception in the script engine here since the result is garbage and we want to let the caller know that
                 } else if (reply.arguments().isEmpty()) {
                     qDebug() << Q_FUNC_INFO << reply.arguments();
                     *reinterpret_cast<bool*>(_a[0]) = 0; // this should really throw an exception in the script engine here since the result is garbage and we want to let the caller know that
                 } else {

                     QVariant v = reply.arguments().first();
                     *reinterpret_cast<bool*>(_a[0]) = v.value<bool>();  // This is the devil in the details.  We need to somehow marshall this shit in.
                     // see https://github.com/nemomobile/nemo-qml-plugin-dbus/blob/master/src/declarativedbusinterface.cpp for some magic on that.
                 }
            } else if ( _id == 1 ) {
                QDBusMessage reply = m_remoteApp->callWithArgumentList(QDBus::BlockWithGui, "ListNames",QVariantList()); // method name from index, args from signature to QVariants
                // in QML we should realy put this to a signal and then invoke a callback when finished for methods
                if (reply.type() == QDBusMessage::ErrorMessage) {
                    qDebug() << Q_FUNC_INFO << reply.errorMessage();
                    *reinterpret_cast<QStringList*>(_a[0]) = QStringList(); // this should really throw an exception in the script engine here since the result is garbage and we want to let the caller know that
                } else if (reply.arguments().isEmpty()) {
                    qDebug() << Q_FUNC_INFO << reply.arguments();
                    *reinterpret_cast<QStringList*>(_a[0]) = QStringList(); // this should really throw an exception in the script engine here since the result is garbage and we want to let the caller know that
                } else {

                    QVariant v = reply.arguments().first();
                    *reinterpret_cast<QStringList*>(_a[0]) = v.value<QStringList>();  // This is the devil in the details.  We need to somehow marshall this shit in.
                    // see https://github.com/nemomobile/nemo-qml-plugin-dbus/blob/master/src/declarativedbusinterface.cpp for some magic on that.
                }
            }
            _id -= 2; // needs to be the total number of methods from the MetaObject
        }
    }

    //
    return _id;
}

void *DbusProxyObject::qt_metacast(const char *cname)
{
    if (!cname) return 0;
    if (!strcmp(cname, "DbusProxyObject")) // Should this really be the proxy name?  Does it matter?
        return static_cast<void*>(const_cast< DbusProxyObject*>(this));
    return QObject::qt_metacast(cname);
}
