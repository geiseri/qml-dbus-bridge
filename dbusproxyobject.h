#ifndef DBUSPROXYOBJECT_H
#include <QtCore/qmetatype.h>

#include <QtQml/QQmlParserStatus>
class QDBusInterface;
class DbusProxyObject : public QObject, public QQmlParserStatus
{
    Q_OBJECT_FAKE
    Q_INTERFACES(QQmlParserStatus)

public:
    explicit DbusProxyObject(const QString &interface, QObject *parent = 0);
    ~DbusProxyObject();

    virtual void classBegin();
    virtual void componentComplete();
private:
    QMetaObject *m_myMeta;
    QDBusInterface *m_remoteApp;
    QByteArray m_proxyName;

};

#endif // DBUSPROXYOBJECT_H
