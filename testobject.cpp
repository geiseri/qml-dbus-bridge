#include "testobject.h"

#include <private/qmetaobjectbuilder_p.h>

TestObject::TestObject(QObject *parent) :
    QObject(parent)
{
    QMetaObjectBuilder builder;
    builder.setSuperClass(&QObject::staticMetaObject);
    builder.setClassName("TestObject");

    builder.addProperty("staticText", "QString");

    m_myMeta = builder.toMetaObject();
}

TestObject::~TestObject()
{
    delete m_myMeta;
}

QString TestObject::staticText() const
{
    return QLatin1String("This is static text from C++");
}

const QMetaObject *TestObject::metaObject() const
{
    qDebug("get meta object");
    return m_myMeta;
}

int TestObject::qt_metacall(QMetaObject::Call c, int id, void **a)
{
    qDebug("Call %d index: %d", c, id);
    id = QObject::qt_metacall(c,id,a);
    if ( id < 0 ) {
        return id;
    } else {
        switch ( c ) {
        case QMetaObject::WriteProperty: {
            void *arg1 = a[0];
            switch ( id ) {
            case 0: {
                qDebug("write %d %s", id, qPrintable(*reinterpret_cast<QString*>(arg1)));
                break;
            }
            }
            break;
        }
        case QMetaObject::ReadProperty: {
            void *arg1 = a[0];
            switch( id ) {
            case 0: {
                qDebug("read %d", id);
                *reinterpret_cast<QString*>(arg1) = QString(QLatin1String("this is derp!"));
                break;
            }
            }

            break;
        }
        default:
            return id;
            break;
        }

        return id;
    }
}
