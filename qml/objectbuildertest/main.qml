import QtQuick 2.0

Rectangle {
    width: 360
    height: 360
    Text {
        id: txt
        text: testObject.staticText
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        MouseArea {
            anchors.fill: parent
            onClicked: {
                console.log("pressed")
                txt.text = "Item: " + dbus.NameHasOwner("org.kde.kded");
            }
        }
    }
    Text {
        id: txt1
        text: testObject.staticText
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        MouseArea {
            anchors.fill: parent
            onClicked: {
                console.log("pressed2")
                rep1.model = dbus.ListNames();
            }
        }
    }
    Column {
        anchors.top: txt.bottom
        anchors.bottom: txt1.top
        anchors.left: parent.left
        anchors.right: parent.right
        clip: true
        Repeater {
            id: rep1
            Text {
                text: modelData
            }
        }
    }

}
