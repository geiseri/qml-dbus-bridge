#ifndef TESTOBJECT_H
#define TESTOBJECT_H
#include <QtCore/QObject>
#include <QtCore/qmetatype.h>
#include <QMetaMethod>
#include <QDBusMessage>
#include <QDBusConnection>
#include <QDBusObjectPath>
#include <QDBusSignature>
#include <QDBusPendingCallWatcher>
#include <QDBusPendingReply>

class TestObject : public QObject
{

public:
    explicit TestObject(QObject *parent = 0);
    virtual ~TestObject();

    QString staticText() const;

signals:

public slots:

private:
    // Pretend to be a Q_OBJECT
    const QMetaObject *metaObject() const;
    int qt_metacall(QMetaObject::Call, int, void **);

    QMetaObject *m_myMeta;
};

#endif // TESTOBJECT_H
