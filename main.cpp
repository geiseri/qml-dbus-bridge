#include <QtGui/QGuiApplication>
#include <QtQml/QQmlEngine>
#include <QtQml/QQmlContext>
#include "qtquick2applicationviewer.h"

#include "testobject.h"
#include "dbusproxyobject.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    TestObject *obj = new TestObject;
    DbusProxyObject *proxy = new DbusProxyObject(QLatin1String("org.freedesktop.DBus"));

    QtQuick2ApplicationViewer viewer;
    viewer.rootContext()->setContextProperty(QLatin1String("testObject"), obj);
    viewer.rootContext()->setContextProperty(QLatin1String("dbus"), proxy); // this needs to be a creatable type
    viewer.setMainQmlFile(QStringLiteral("qml/objectbuildertest/main.qml"));
    viewer.showExpanded();

    return app.exec();
}
